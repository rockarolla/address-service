package mx.rulo4.spring.microservices.addressservice.address;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(
    uniqueConstraints =
        @UniqueConstraint(
            columnNames = {
              "houseNumber",
              "street",
              "streetType",
              "zipCode",
              "city",
              "state",
              "country"
            }))
public class Address {
  @Id @GeneratedValue @JsonIgnore private Long id;

  @NotNull(message = "House number is required")
  @Size(min = 1, max = 15, message = "House number must contain between 3 and 15 characters")
  private String houseNumber;

  @NotNull(message = "Street is required")
  @Size(min = 3, max = 100, message = "Street name must contain between 3 and 100 characters")
  private String street;

  @NotNull(message = "Street type is required")
  @Size(min = 3, max = 10, message = "Street type must contain between 3 and 10 characters")
  private String streetType;

  @NotNull(message = "Zip code is required")
  @Pattern(regexp = "[0-9]{5,9}", message = "Street name must contain between 5 and 9 digits")
  private String zipCode;

  @NotNull(message = "City is required")
  @Size(min = 3, max = 80, message = "City must contain between 3 and 80 characters")
  private String city;

  @NotNull(message = "State name is required")
  @Size(min = 3, max = 80, message = "State must contain between 3 and 80 characters")
  private String state;

  @NotNull(message = "Country is required")
  @Size(min = 3, max = 80, message = "Country must contain between 3 and 80 characters")
  private String country;
}
