package mx.rulo4.spring.microservices.addressservice.filters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/persons/*")
@Component
public class AddResponseHeaderFilter implements Filter {

  @Autowired private Environment environment;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {}

  private void addServerPortHeader(ServletResponse response) {
    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
    httpServletResponse.setHeader("Server-Port", environment.getProperty("local.server.port"));
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    addServerPortHeader(response);
    chain.doFilter(request, response);
  }

  @Override
  public void destroy() {}
}
