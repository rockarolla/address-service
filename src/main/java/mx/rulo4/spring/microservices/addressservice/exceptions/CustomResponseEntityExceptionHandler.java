package mx.rulo4.spring.microservices.addressservice.exceptions;

import mx.rulo4.spring.microservices.addressservice.address.exceptions.AddressNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(AddressNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public final ResponseEntity<Object> handlePersonNotFoundException(
      Exception exception, WebRequest webRequest) {
    ExceptionResponse exceptionResponse =
        new ExceptionResponse(
            Instant.now(), exception.getLocalizedMessage(), webRequest.getDescription(false));
    return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException exception,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest webRequest) {

    List<String> errors =
        exception.getBindingResult().getFieldErrors().stream()
            .map(error -> error.getDefaultMessage())
            .collect(Collectors.toList());

    ExceptionResponse exceptionResponse =
        new ExceptionResponse(Instant.now(), "Invalid data sent", errors.toString());

    return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
  }
}
