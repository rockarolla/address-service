package mx.rulo4.spring.microservices.addressservice.address;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {

  Address findAddressByCountryAndStateAndCityAndZipCodeAndStreetTypeAndStreetAndHouseNumber(
      String country,
      String state,
      String city,
      String zipCode,
      String streetType,
      String street,
      String houseNumber);
}
