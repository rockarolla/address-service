package mx.rulo4.spring.microservices.addressservice.address.exceptions;

public class AddressNotFoundException extends RuntimeException {
  
  private static final String ADDRESS_NOT_FOUND_MESSAGE = "Doesn't exist address with id: %s";
  
  public AddressNotFoundException(Long id) {
    super(String.format(ADDRESS_NOT_FOUND_MESSAGE, id));
  }
}
