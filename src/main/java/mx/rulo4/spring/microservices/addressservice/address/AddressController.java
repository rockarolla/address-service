package mx.rulo4.spring.microservices.addressservice.address;

import mx.rulo4.spring.microservices.addressservice.address.exceptions.AddressNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class AddressController {

  @Autowired private AddressRepository addressRepository;

  @GetMapping("/addresses")
  public List<Address> getAll() {
    return this.addressRepository.findAll();
  }

  @GetMapping("/addresses/{id}")
  public Address get(@PathVariable Long id) {
    Optional<Address> adddressFound = this.addressRepository.findById(id);

    if (!adddressFound.isPresent()) {
      throw new AddressNotFoundException(id);
    } else {
      return adddressFound.get();
    }
  }

  @PostMapping("/addresses")
  public ResponseEntity<Long> post(@Valid @RequestBody Address address) {

    Address existingAddress =
        this.addressRepository
            .findAddressByCountryAndStateAndCityAndZipCodeAndStreetTypeAndStreetAndHouseNumber(
                address.getCountry(),
                address.getState(),
                address.getCity(),
                address.getZipCode(),
                address.getStreetType(),
                address.getStreet(),
                address.getHouseNumber());
  
    Address addressSaved;
    if (existingAddress != null) {
      addressSaved = existingAddress;
    } else {
      addressSaved = addressRepository.save(address);
    }

    URI createdLocation =
        ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(addressSaved.getId())
            .toUri();

    return ResponseEntity.created(createdLocation).body(addressSaved.getId());
  }

  @DeleteMapping("/addresses/{id}")
  public void delete(@PathVariable Long id) {
    Optional<Address> addressToDelete = this.addressRepository.findById(id);

    if (!addressToDelete.isPresent()) {
      throw new AddressNotFoundException(id);
    } else {
      this.addressRepository.delete(addressToDelete.get());
    }
  }
}
